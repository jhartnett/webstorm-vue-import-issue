import Vue from 'vue';
import App from './App';
import Dummy from './Dummy';

Vue.config.productionTip = false;
if(false)
    Vue.component('dummy', Dummy);

new Vue({
    render: h => h(App)
}).$mount('#app');
