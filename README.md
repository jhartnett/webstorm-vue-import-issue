# WebStorm Automatic Import Issue

This project demonstrates an issue with WebStorm's automatic importing of referenced Vue components. 
This is likely due to the fix for [this issue](https://youtrack.jetbrains.com/issue/WEB-28512?_ga=2.170741539.224012963.1596826451-1005407315.1569199717) being too aggressive.
Relevant discussion [here](https://intellij-support.jetbrains.com/hc/en-us/community/posts/360006006560-PHPStorm-WebStorm-does-not-add-import-statements-to-vue-component-from-library).

### Repro Steps

1. Clone this repo locally
    ```bash
    git clone git@gitlab.com:jhartnett/webstorm-vue-import-issue.git
    cd webstorm-vue-import-issue
    ```
1. Install dependencies
    ```bash
    npm install
    ```
1. Open the project in WebStorm
1. Open `src/App.vue`
1. Delete and start to retype `<dummy/>`
1. When the `dummy` component is suggested by autocompletion, hit enter to autocomplete.

### Expected Behavior

WebStorm automatically imports the `Dummy` component from `src/Dummy.vue` and includes it in the `components` object of the vue `App` component.

### Actual Behavior

WebStorm does not automatically import the component.